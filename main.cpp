#include <iostream>
#include "pessoa.hpp"

using namespace std;

// main file
int main () {
	
	Pessoa umaPessoa;
	Pessoa outraPessoa;
		
	cout << "Nome: " << umaPessoa.getNome() << endl;
	cout << "Idade: " << umaPessoa.getIdade() << endl;
	cout << "Telefone: " << umaPessoa.getTelefone() << endl;

	outraPessoa.setNome("Paulo");
	outraPessoa.setIdade("32");
	outraPessoa.setTelefone("555-5555");
	
	cout << "Nome: " << outraPessoa.getNome() << endl;
        cout << "Idade: " << outraPessoa.getIdade() << endl;
	cout << "Telefone: " << outraPessoa.getTelefone() << endl;

	
	return 0;
}
